---
title: Cloud Native 
description: >- 
  We provide a collaborative environment for the world’s leading Java ecosystem
  players to advance open source enterprise Java technologies for the cloud.
header_wrapper_class: header-topic
jumbotron_container: container-fluid
jumbotron_class: " "
custom_jumbotron: |
  <div class="custom-jumbotron-main container">
    <h1 class="featured-jumbotron-headline">Cloud Native</h1>
    <p>
      We provide a collaborative environment for the world’s leading Java
      ecosystem players to advance open source enterprise Java technologies for
      the cloud.
    </p>
  </div>
custom_jumbotron_class: col-md-24
hide_jumbotron: false
show_featured_story: true
hide_breadcrumb: true
hide_sidebar: true
hide_page_title: true
page_css_file: /public/css/topics.css
---

{{< grid/div class="padding-top-60 padding-bottom-40" isMarkdown="false" >}}
    <h2 class="margin-bottom-40" id="projects">Projects</h2>
    <div 
      class="featured-projects display-flex margin-top-40" 
      data-types="projects" 
      data-url="https://projects.eclipse.org/api/projects?technology_types=cloud%20native%20java" 
      data-template-id="tpl-project-basic-card" data-page-size="3" 
      data-sorting-method="random"
    >
    </div>

{{</ grid/div >}}
{{< mustache_js template-id="tpl-project-basic-card" path="/js/src/templates/tpl-project-basic-card.mustache" >}}

{{< pages/topics/communities topic="cloud_native" >}}

{{< grid/div class="padding-top-60 padding-bottom-40" isMarkdown="false" >}}
    <h2 class="margin-bottom-40" id="insights-and-resources">Insights & Resources</h2>
    {{< newsroom/resources wg="adoptium, jakarta_ee" type="case_study, white_paper, market_report, social_media_kit" template="cover" limit="3" >}}
{{</ grid/div >}}

