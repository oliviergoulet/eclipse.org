---
title: Automotive & Mobility 
description: >- 
  We provide leading automotive OEMs, their suppliers, and partners with a
  sustainable, transparent, and vendor-neutral platform to collaborate on open
  technologies and standards.
header_wrapper_class: header-topic
jumbotron_container: container-fluid
jumbotron_class: " "
custom_jumbotron: |
  <div class="custom-jumbotron-main container">
    <h1 class="featured-jumbotron-headline">Automotive & Mobility</h1> 
    <p>
      We provide leading automotive OEMs, their suppliers, and partners with a
      sustainable, transparent, and vendor-neutral platform to collaborate on open
      technologies and standards.
    </p>
  </div>
custom_jumbotron_class: col-md-24
hide_jumbotron: false
show_featured_story: true
hide_breadcrumb: true
hide_sidebar: true
hide_page_title: true
container: container margin-bottom-40
page_css_file: /public/css/topics.css
---

{{< grid/div class="padding-top-60 padding-bottom-40" isMarkdown="false" >}}
    <h2 class="margin-bottom-40" id="projects">Projects</h2>
    <div 
      class="featured-projects display-flex margin-top-40" 
      data-types="projects" 
      data-url="https://projects.eclipse.org/api/projects?technology_types=automotive" 
      data-template-id="tpl-project-basic-card" data-page-size="3" 
      data-sorting-method="random"
    >
    </div>

{{</ grid/div >}}
{{< mustache_js template-id="tpl-project-basic-card" path="/js/src/templates/tpl-project-basic-card.mustache" >}}

{{< pages/topics/communities topic="automotive_and_mobility" >}}

