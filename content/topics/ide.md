---
title: Developer Tools & IDE
description: >- 
    Our community is innovating on the next generation of cloud native
    developer tools, including the Eclipse IDE which is the leading open
    platform for professional developers.
header_wrapper_class: header-topic
jumbotron_container: container-fluid
jumbotron_class: " "
custom_jumbotron: |
    <div class="custom-jumbotron-main container">
        <h1 class="featured-jumbotron-headline">Developer Tools & IDE</h1>
        <p>
           Our community is innovating on the next generation of cloud native
           developer tools, including the Eclipse IDE which is the leading open
           platform for professional developers. 
        </p>
    </div>
custom_jumbotron_class: col-md-24
hide_jumbotron: false
show_featured_story: true
hide_breadcrumb: true
hide_sidebar: true
hide_page_title: true
page_css_file: /public/css/topics.css
---

{{< grid/div class="padding-top-60 padding-bottom-40" isMarkdown="false" >}}

    <h2 class="margin-bottom-40" id="projects">Projects</h2>
    <div 
      class="featured-projects display-flex margin-top-40" 
      data-types="projects" 
      data-url="https://projects.eclipse.org/api/projects?technology_types=tools" 
      data-template-id="tpl-project-basic-card" data-page-size="3" 
      data-sorting-method="random"
    >
    </div>

{{</ grid/div >}}
{{< mustache_js template-id="tpl-project-basic-card" path="/js/src/templates/tpl-project-basic-card.mustache" >}}

{{< pages/topics/communities topic="ide" >}}

{{< grid/div class="padding-top-60 padding-bottom-40" isMarkdown="false" >}}
    <h2 class="margin-bottom-40" id="insights-and-resources">Insights & Resources</h2>
    {{< newsroom/resources wg="ecd_tools,edge_native" type="case_study, white_paper, market_report, social_media_kit" template="cover" limit="3" >}}
{{</ grid/div >}}
