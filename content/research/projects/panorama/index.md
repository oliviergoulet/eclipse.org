---
title: "PANORAMA"
date: 2019-04-01T00:00:00-00:00
project_logo: "/research/images/research/r-projects/panorama.png"
tags: ["System of Systems", "Modeling","Embedded systems","Automotive", "Aerospace","Systems design"]
homepage: "https://www.panorama-research.org/"
facebook: ""
linkedin: ""
twitter: "https://twitter.com/PanoramaEng"
youtube: "https://www.youtube.com/watch?v=3GMcZeRXASQ"
funding_bodies: ["itea3", "bmbf"]
eclipse_projects: ["automotive.app4mc","modeling.capra"]
project_topic: "Automotive"
summary: "Accelerating design efficiency for Automotive and aerospace systems"
hide_page_title: true
hide_sidebar: true
header_wrapper_class: "header-projects-bg-img"
description: "# **PANORAMA**

The Panorama project boosts design efficiency for heterogeneous automotive and aerospace systems. Based in open source, it provides an environment for collaboration amongst diverse hardware and software technologies and teams, especially at the early stages of design. It supports efficient design decisions by defining evolving standards, tools and best practises for exchange of non-functional, formal models..

The main activities of the project will extend the scope of current system level approaches by enhancing existing abstract performance meta-models to be suitable for heterogeneous hardware, and heterogeneous function domains. We will stand on the shoulder of giants by building on the meta-model developed in the AMALTHEA and AMALTHEA4 public projects, taking results from projects such as TIMMO, Timmo2USE, ARAMiS I & II into account. The enhanced meta-model will be a common and open standard to support development by diverse parties across organizations.


The project was running from April 2019 to March 2022"
---

{{< grid/div class="container research-page-section">}}

[PANORAMA flyer](panorama-flyer-2019-print.pdf)

{{</ grid/div>}}

{{< grid/div class="bg-light-gray" isMarkdown="false">}}
{{< grid/div class="container research-page-section" >}}

# Consortium

* Arcticus Systems
* AVL
* Bosch
* Critical Software
* Eclipse Foundation Europe
* Dortmund University of Applied Sciences and Arts
* Fraunhofer IEM
* Inchron
* KTH Royal Institute of Technology
* Mantis Software Company
* Mälardalen University
* Polytechnic Institute of Porto
* OFFIS
* Qamcom
* Regensburg University of Applied Sciences
* SAAB
* Siemens
* Siili
* TactoTek
* UNIT Information Technologies
* University of Gothenburg
* University of Rostock
* Vector

{{</ grid/div>}}
{{</ grid/div>}}
